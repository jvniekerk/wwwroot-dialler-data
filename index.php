<?php

include_once 'resources/keys.php';
include_once 'resources/helpers.class.php';


/**
 * Page URL
 */
$url = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];


$helpers = new helpers();

try {
    $pdo = new PDO(UB_CONNECTION_STRING, UB_LAPS_DB_USERNAME, UB_LAPS_DB_PASSWORD);
} catch (Exception $e) {
    print_r($e->getMessage());
}


$page_title = 'Ultra Dialler Integration Data';


/**
 * Array for different report page sections
 */
$functions = array(
    array('name' => 'Completed Runs', 'function' => 'fn_DataView_completedRuns'),
    array('name' => 'Average Duration', 'function' => 'fn_DataView_med'),
    array('name' => 'Calls', 'function' => 'fn_DataView_calls'),
    array('name' => 'Dispositions', 'function' => 'fn_DataView_dispo'),
    array('name' => 'Old Accounts Deleted', 'function' => 'fn_DataView_oldAccounts'),
    array('name' => 'Agent Activities', 'function' => 'fn_DataView_agentActivities'),
    array('name' => 'List Account States', 'function' => 'fn_DataView_lists'),
    array('name' => 'Procedures Run', 'function' => 'fn_DataView_procRun'),
    array('name' => 'Parsed XML', 'function' => 'fn_DataView_parsedXML'),
    array('name' => 'Ultra Downloads', 'function' => 'fn_DataView_download'),
    array('name' => 'Integration Runs', 'function' => 'fn_DataView_integration')
);

/**
 * Large report sections that need to be hidden
 */
$reduce = array(
    'fn_DataView_procRun',
    'fn_DataView_parsedXML',
    'fn_DataView_download',
    'fn_DataView_integration'
);


/**
 * Set up HTML start & head & headers & CSS & page title
 */
$refresh_btn = "
        <form method='get' action='$url'>
            <input type='submit' value='Refresh Page' class='btn btn-primary'>
        </form>";


/**
 * Set up button to go to top of page
 */
$top_button = "
    <div>
        <a class='btn btn-primary pull-right' href='#page-title' style='margin: 1em;'>Go to top</a>
    </div>";
//<a class='btn btn-primary' href='http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . "#page-title'>Go to top</a>


/**
 * Set up menu with links to sections
 */

$link_btn = "class='btn btn-success' style='margin: 0.2em;'";

$menu = "
            <h3 class='h3' style='font-weight: bold; text-align: center;'>Menu</h3>
            
            <ol>";

foreach ($functions as $function) {
    $menu .= "
                <li><a href='#" . str_replace(' ', '_',
                                              $function['name']) . "-header' $link_btn>" . $function['name'] . "</a></li>";
}

$menu .= "
                <li><a href='#errors' $link_btn>Upload Errors</a></li>
            </ol>";


/**
 * Add in scripts
 *
 * Table hide/un-hide functionality     -> showTable(id)
 * Toggle XML field values              -> showField(id)
 * Scroll to Batch error report header  -> error_tbl
 * Scroll top of page                   -> gotoTop()
 */
$js = "        
         <!-- Latest compiled and minified CSS -->
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
        
        <!-- jQuery library -->
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
        
        <!-- Latest compiled JavaScript -->
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script> 
          
        
        <script type='text/javascript'>
            function showTable(id) {
                if (id.style.display === 'none') {
                    id.style.display = 'block';
                } else {
                    id.style.display = 'none';
                }
            }
            
            
            function showField(id) {
                let field = document.getElementById(id);
                
                if (field.style.display === 'none') {
                    field.style.display = 'block';
                } else {
                    field.style.display = 'none';
                }
            }
            
            
            let error_tbl = document.getElementById('batchID_header');
            
            if (error_tbl) {
                error_tbl.scrollIntoView(); 
            } 
        </script>";


/**
 * Placeholder div
 */
$placeholder = "<div class='placeholder' style='margin-bottom: 2em;'></div>";
/**
 * Build page HTML
 */
$html = "
<!DOCTYPE html>
<html lang='en'>

<head>
    <title>$page_title</title>
            
    <meta>
    <meta name='viewport' content='width=device-width, initial-scale=1' charset='UTF-8'>
</head>

<body style='margin: 2em;'>
    <div id='page-title'></div>
    
    <div class='container-fluid'>      
        <h1 class='h1 text-uppercase jumbotron text-center' style='font-weight: bold;'>$page_title</h1>
        
        
        <div class='row'>
            <div class='col-md-2 col-md-offset-5' style='background-color: cadetblue; border-radius: 15px;'>
                $menu
                
                $placeholder
                <div class='col-sm-2 col-md-offset-2'>
                    $refresh_btn
                    
                    $placeholder
                </div>
            
                $placeholder
            </div>
        </div>
    </div>
    
    $placeholder
    
    <div class='container-fluid' style='background-color: darkseagreen; border-radius: 15px;'>
        ";


/**
 * Create the tables with the data views
 */
foreach ($functions as $function) {
    $top = in_array($function['function'], $reduce) ? ' top 100' : '';

    $sql = "select $top * from " . UB_LAPS_DIALLER_NAME . '.dbo.' . $function['function'] . '()';

    $stmt = $pdo->query($sql);

    $tables = $stmt->fetchall(PDO::FETCH_ASSOC);

    $table = "
        <h2 id='" . str_replace(' ', '_',
                                $function['name']) . "-header' style='font-weight: bold;'>" . $function['name'] . "</h2>
    " . $helpers->get_table($tables);


    /**
     * Check if table should be hidden with functionality to un-hide table to view
     */
    if (in_array($function['function'], $reduce)) {
        $table = str_replace("<table class='table  table-bordered'>",
                             "<table class='table  table-bordered' style='display: none;' id='" . str_replace(' ', '_',
                                                                                                              $function['name']) . "'>",
                             $table);


        /**
         * Add button to toggle hide/un-hide functionality
         */
        $onclick = "onclick='showTable(" . str_replace(' ', '_', $function['name']) . ")'";

        $table = str_replace('</h2>', "</h2>
        <button class='btn btn-primary' $onclick style='margin-bottom: 1em;'>Toggle Table</button>
        ", $table);
    }

    $html .= "$table

";
}


/**
 * Functionality to view Ultra upload errors
 */
$batchID = (!empty($_POST['batchID'])) ? $_POST['batchID'] : '';    //845682


/**
 * Show errors for today
 */
$table = '';


/**
 * Start new errors section
 */
$batchView = "
        <br>
        <h1 id='errors' class='text-uppercase' style='font-weight: bold;'>Upload Errors</h1>
        ";


/**
 * Error section functions for data from db
 */
$functions = array();

$functions = array(
    array('name' => 'Errors Today', 'function' => 'fn_DataView_getErrorsToday()')
);


/**
 * Build each errors section from functions
 */
foreach ($functions as $function) {
    $sql = "select * from " . UB_LAPS_DIALLER_NAME . '.dbo.' . $function['function'];

    $stmt = $pdo->query($sql);

    $tables = $stmt->fetchall(PDO::FETCH_ASSOC);

    $table = "
        <h2 style='font-weight: bold;'>" . $function['name'] . "</h2>
    ";

    if (!empty($tables)) {
        $table .= $helpers->get_table($tables);
    } else {
        if ($function['function'] == 'fn_DataView_getErrorsToday()') {
            $table .= "        <div>No upload errors today</div>";
        }
    }

    $batchView .= "
        $table";
}

$html .= $batchView;


/**
 * Show errors for batchID entered
 */
$table = '';

$batchView = "
        <br><br>
 
        <form method='post'>                 
            <label for='batchID'>Batch ID:</label>
            <input type='text' class='form-control' value='$batchID' name='batchID' id='batchID' style='margin-bottom: 0.5em; width: 10em;'>
            <input type='submit' value='Get Error Batch Data' class='btn btn-primary'>
        </form>";


/**
 * Show errors for input Batch ID
 */
if (!empty($_POST['batchID'])) {
    $functions = array();

    $functions = array(
        array('name' => "Errors for BatchID $batchID", 'function' => "fn_DataView_getUploadErrors(:batchID)")
    );

    /**
     * Show data for each function
     */
    foreach ($functions as $function) {
        $sql = "select * from " . UB_LAPS_DIALLER_NAME . '.dbo.' . $function['function'];

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam('batchID', $batchID);
        $stmt->execute();

        $tables = $stmt->fetchall(PDO::FETCH_ASSOC);

        /**
         * If there are errors, show them in table
         */
        if (!empty($tables)) {
            $table = "
         <h2 id='batchID_header' style='font-weight: bold;'>" . $function['name'] . "</h2>
     " . $helpers->get_table($tables);
        } else {
            $table = "        $placeholder
        <div id='batchID_header'>No errors for BatchID $batchID</div>";
        }

        $batchView .= "
 
 $table";
    }
}

$html .= $batchView;


/**
 * Set up footer
 */
$footer = "
    <footer>
        $js
    </footer>";


/**
 * Close off body
 */
$html .= "
        
        <br><br>
        
        <div class='form-group'>
            <div class='row'>
                <div class='col-md-2 col-md-offset-5'>$refresh_btn</div>
            </div>
        </div>
    </div>
     
    $footer
    
    $top_button
</body>
    ";


/**
 * Close off HTML
 */
$html .= "   
</html>
";


/**
 * Add top navigation button after section headings
 */
$html = str_replace("</h2>", "</h2>
        $top_button", $html);


/**
 * Display page
 */
echo($html);