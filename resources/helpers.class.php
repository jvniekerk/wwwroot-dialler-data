<?php
/**
 * Created by PhpStorm.
 * User: jvniekerk
 * Date: 04/01/2019
 * Time: 15:01
 */

class helpers {
    /**
     * Get headers from a multi-dimensional array
     *
     * @param $array
     *
     * @return array
     */
    public function get_array_headers($array) {
        $temp_array = array();

        foreach ($array as $sub_array) {
            foreach ($sub_array as $key => $value) {
                !in_array($key, $temp_array) ? array_push($temp_array, $key) : "";
            }
        }

        return $temp_array;
    }


    /**
     * Returns a table built from input array data
     *
     * @param $data
     *
     * @return string
     */
    public function get_table($data) {
        /**
         * Open the table
         */
        $html = "
        <div class='table-responsive'>
            <table class='table  table-bordered'>
                <thead>";

        $headers = $this->get_array_headers($data);

        $html .= '
                    <tr>';

        /**
         * Add the headers in
         */
        foreach ($headers as $header) {
            $html .= "
                        <td>$header</td>";
        }

        $html .= '
                    </tr>
                </thead>
                <tbody>';


        /**
         * Variables for hiding large chunks of data and assigning IDs
         */
        $btn = '';
        $i = 1;


        /**
         * Add the data rows
         */
        foreach ($data as $datum) {
            $html .= '
                    <tr>';

            foreach ($datum as $key => $value) {
                if ($key == 'UploadErrorID') {
                    $id = $value;
                }

                if ($key == 'ResponseXML') {
                    $html .= "<td id='$id' style='display: none;'>" . htmlentities($value) . "</td>";

                    $br = (!fmod($i, 3)) ? '<br>' : '';
                    $btn .= "<button class='btn btn-primary' onclick='showField($id)'>Toggle UploadErrorID $id XML</button>$br";
                } else {
                    $html .= "<td>$value</td>";
                }
            }

            $i++;

            $html .= "</tr>";
        }


        /**
         * Close the table
         */
        $html .= "
                </tbody>
            </table>  
        </div>
        $btn     
        ";

        return $html;
    }


    /**
     * CSS for the HTML report page
     */
    public function get_table_css() {
        $css = '<style>
                table, tr, td {
                    border: 1px solid black;
                    border-collapse: collapse;
                    padding: 0.5em;
                }
                
                button, .btn {
                    background-color: #eee;
                    color: black;
                    padding: 0.5em;
                    border: solid black;
                    text-align: center;
                    font-size: 15px;
                    margin: 0.2em;
                    border-radius: 12px;
                }
            </style>';

        return $css;
    }
}